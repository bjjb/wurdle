/**
 * The default layout of a Scrabble board. `W`, `w`, `L`, `l` and `*` mean 
 * TWS (triple-word score), DWS (double-word score), TLS (triple-letter
 * score), DLS (double-letter score) and start respectively, while `.`
 * represents an ordinary square.
 */
const BOARD = `
W..l...W...l..W
.w...L...L...w.
..w...l.l...w..
l..w...l...w..l
....w.....w....
.L...L...L...L.
..l...l.l...l..
W..l...*...l..W
..l...l.l...l..
.L...L...L...L.
....w.....w....
l..w...l...w..l
..w...l.l...w..
.w...L...L...w.
W..l...W...l..W
`

/**
 * Maps symbols from a board definition to element classList names.
 */
const CLASSES = {
  '*': ['start', 'dws'],  // center square; double-word score
  'w': ['dws'],           // double-word score
  'W': ['tws'],           // triple word score
  'l': ['dls'],           // double-letter score
  'L': ['tls'],           // triple-letter score
  '.': [],                // no special attributes
}

/**
 * Gets a Scrabble square identifier for a given x and y value. For example,
 * (0, 0) should be A1, (7, 7) should be H8, (3, 11) should be C12, etc.
 */
function cellID(x, y) {
  return `${(10 + Number(x)).toString(36).toUpperCase()}${1 + Number(y)}`
}

/**
 * Generates squares from the board. Each square is an element of type
 * nodeType whose attributes are based on the character at each point in the
 * board (the ID, the class, the data-word, data-letter,the data-x and data-y
 * values).
*/
function* squares(board = BOARD, nodeType = 'scrabble-square') {
  const rows = board.trim().split(/\n+/)
  const n = rows.length
  for (const y in rows) {
    const row = rows[y].trim()
    if (row.length !== n) throw new Exception('board is not square')
    for (const x in row) {
      const c = row[x]
      const element = document.createElement(nodeType)
      element.id = cellID(x, y)
      element.classList.add(...(CLASSES[c] ?? []))
      yield element
    }
  }
}

/**
 * A Board represents a Scrabble board. It consists of squares with particular
 * classes and attributes, organised into a grid. Squares may have multiplier
 * values such as TWS (triple word score). A square may hold a Tile, which has
 * a value (a letter) and an intrinsic score. The Board can be used to
 * calculate scores for inteconnected tiles in rows of columns.
 */
class Board extends HTMLElement {
  static get squares() {
    return squares(BOARD)
  }

  /**
   * Makes a new Scrabble board. Populates itself with squares and listens for
   * events on each square.
   */
  constructor() {
    super()
    for (const square of Board.squares) {
      square.addEventListener('click', this)
      square.addEventListener('mouseover', this)
      square.addEventListener('mouseout', this)
      this.append(square)
    }
    addEventListener('keydown', this)
    addEventListener('fullscreenchange', this)
    addEventListener('fullscreenerror', this)
  }

  get squares() {
    return this.querySelectorAll('> *')
  }

  get rows() {
    return this.squares.reduce((rows, square) => {
      (rows[square.y] ??= []).push(square)
      rows
    }, [])
  }

  get cols() {
    return this.squares.reduce((cols, square) => {
      (rows[square.x] ??= []).push(square)
      cols
    }, [])
  }

  handleEvent(event) {
    const { target: { dataset: id } } = event
    if (!id) return
    event.preventDefault()
    console.debug(event)
  }
}

class Square extends HTMLElement {
  #x
  #y
  #wordScore
  #letterScore

  static wordScore(type) { return { 'W': 3, 'w': 2, '*': 2 }[type] ?? 1 }
  static letterScore(type) { return { 'L': 3, 'l': 2 }[type] ?? 1 }
  static id(x, y) { return cellID(x, y) }

  constructor(type, x, y) {
    super()
    this.classList.append(...Square.classListFor(type))
    this.#x = x
    this.#y = y
    this.#wordScore = Square.word(type)
    this.#letterScore = Square.letter(type)
    this.id = Square.id(x, y)
  }

  get x() {
    return this.#x
  }

  get y() {
    return this.#y
  }

  get wordScore() {
    return this.#wordScore
  }

  get letterScore() {
    return this.#letterScore
  }
}

export { Board };
